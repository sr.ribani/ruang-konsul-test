import 'package:flutter/cupertino.dart';
import 'package:ruang_konsul_test/util/app_color.dart';

class PageIndicator extends StatefulWidget {

  final int count;
  final PageController controller;
  const PageIndicator({super.key, required this.count, required this.controller});

  @override
  State<StatefulWidget> createState() => _PageIndicatorState();

}

class _PageIndicatorState extends State<PageIndicator> {

  var indicatorList = <_IndicatorIndex>[];
  var selected = 0;

  @override
  void initState() {
    for (var i = 0; i < widget.count; i++) {
      var item = _IndicatorIndex();
      item.index = i;
      item.selected = i == 0;

      indicatorList.add(item);
    }

    widget.controller.addListener(_handler);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        for (var item in indicatorList)
          Flexible(
              child: Padding(
                padding: const EdgeInsets.only(right: 4),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: item.selected ? AppColor.primary : AppColor.disable,
                  ),
                  height: 8,
                ),
              )
          )
      ],
    );
  }

  _handler() {
    var currPage = widget.controller.page?.toInt();
    if (currPage != null) {
      setState(() {
        for (var i = 0; i < indicatorList.length; i++) {
          var item = indicatorList[i];
          item.selected = item.index <= currPage;
        }
      });
    }
  }

}

class _IndicatorIndex {
  late int index = 0;
  late bool selected = false;
}

