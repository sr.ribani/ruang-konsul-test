import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:ruang_konsul_test/util/app_color.dart';

class CardMainInfo extends StatelessWidget {
  const CardMainInfo({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      height: 130,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            children: [
              CircularPercentIndicator(
                radius: 40,
                lineWidth: 12,
                percent: 0.75,
                progressColor: AppColor.secondary,
                backgroundColor: Colors.transparent,
                center: const Text(
                    '75',
                    style: TextStyle(
                      color: AppColor.secondary,
                      fontWeight: FontWeight.w700,
                      fontSize: 24
                    ),
                ),
              ),
              const Flexible(
                  child: Center(
                      child: Text.rich(
                        TextSpan(
                            text: 'Your Overall Skin\nScore is ',
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 18,
                                color: Colors.black
                            ),
                            children: [
                              TextSpan(
                                  text: 'Good',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 18,
                                      color: AppColor.secondary
                                  )
                              )
                            ]
                        ),
                        textAlign: TextAlign.center,
                      )
                  )
              )
            ],
          ),
        ),
      ),
    );
  }
}

class CardSubInfo extends StatelessWidget {

  final String info;
  final int progress;
  const CardSubInfo({super.key, required this.info, required this.progress});

  Color _getColor() {
    if (progress < 50) {
      return AppColor.red;
    } else if (progress < 100) {
      return AppColor.secondary;
    } else {
      return AppColor.green;
    }
  }

  String _getHighlight() {
    if (progress < 50) {
      return 'Bad';
    } else if (progress < 100) {
      return 'Good';
    } else {
      return 'Perfect';
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 142,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              CircularPercentIndicator(
                radius: 25,
                lineWidth: 8,
                percent: progress/100,
                progressColor: _getColor(),
                backgroundColor: Colors.transparent,
                center: Text(
                  '$progress',
                  style: TextStyle(
                      color: _getColor(),
                      fontWeight: FontWeight.w700,
                      fontSize: 14
                  ),
                ),
              ),
              Flexible(
                  child: Center(
                    child: Text.rich(
                      TextSpan(
                          text: 'Your $info\nSkin score is ',
                          style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 12,
                              color: Colors.black
                          ),
                          children: [
                            TextSpan(
                                text: _getHighlight(),
                                style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontSize: 12,
                                    color: _getColor()
                                )
                            )
                          ]
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
              )
            ],
          ),
        ),
      ),
    );
  }

}