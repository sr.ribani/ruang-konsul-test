import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../util/app_color.dart';

class UiKitChip extends StatefulWidget {

  final List<String> list;
  const UiKitChip({super.key, required this.list});

  @override
  State<StatefulWidget> createState() => _UiKitChipState();

}

class _UiKitChipState extends State<UiKitChip> {

  var selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: widget.list.asMap().map((index, element) =>
          MapEntry(
              index,
              Padding(
                padding: const EdgeInsets.only(right: 6),
                child: ChoiceChip(
                  backgroundColor: Colors.transparent,
                  selectedColor: AppColor.primary,
                  shape: const StadiumBorder(
                      side: BorderSide(color: AppColor.primary)
                  ),
                  selected: selectedIndex == index,
                  label: Text(
                    element,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: selectedIndex == index ? Colors.white : AppColor.primary
                    ),
                  ),
                  onSelected: (_) {
                    setState(() {
                      selectedIndex = index;
                    });
                  },
                ),
              )
          )
      ).values.toList(),
    );
  }

}