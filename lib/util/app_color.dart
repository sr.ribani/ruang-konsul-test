import 'dart:ui';

class AppColor {
  static const primary = Color(0xFF9C1E60);
  static const secondary = Color(0xFFFAAF3B);
  static const disable = Color(0xFFececec);
  static const black36 = Color(0x5C2C2829);
  static const grey = Color(0xFFBCBCBC);
  static const red = Color(0xFFeb5757);
  static const green = Color(0xFF28ae60);
}