import 'dart:async';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ruang_konsul_test/component/card.dart';
import 'package:ruang_konsul_test/util/app_color.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class ResultPage extends StatefulWidget {
  
  final XFile picture;
  const ResultPage({super.key, required this.picture});

  @override
  State<StatefulWidget> createState() => _ResultPageState();
  
}

class _ResultPageState extends State<ResultPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Image.file(File(widget.picture.path), fit: BoxFit.fill),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 18, left: 24, right: 24),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () => {
                      context.navigateBack()
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppColor.black36
                      ),
                      width: 36,
                      height: 36,
                      child: Center(
                        child: SvgPicture.asset(
                          'assets/icon/left.json',
                          width: 14,
                          height: 14,
                        ),
                      ),
                    ),
                  ),
                  const Spacer(),
                  Container(
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColor.black36
                    ),
                    width: 36,
                    height: 36,
                    child: Center(
                      child: SvgPicture.asset(
                        'assets/icon/share.svg',
                        width: 14,
                        height: 14,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SlidingUpPanel(
              minHeight: 160,
              panelBuilder: (controller) => ListView(
                controller: controller,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 21),
                    child: Center(
                      child: Container(
                        height: 5,
                        width: 36,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: AppColor.grey
                        ),
                      ),
                    ),
                  ),
                  const CardMainInfo(),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: GridView(
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 6,
                          crossAxisSpacing: 6
                      ),
                      shrinkWrap: true,
                      children: const [
                        CardSubInfo(
                            info: 'Uneven Skintone',
                            progress: 25
                        ),
                        CardSubInfo(
                            info: 'Smoothness',
                            progress: 75
                        ),
                        CardSubInfo(
                            info: 'Radiance',
                            progress: 75
                        ),
                        CardSubInfo(
                            info: 'Shine  Skin',
                            progress: 100
                        ),
                        CardSubInfo(
                            info: 'Radiance',
                            progress: 75
                        ),
                        CardSubInfo(
                            info: 'Uneven Skintone',
                            progress: 25
                        ),
                        CardSubInfo(
                            info: 'Uneven Skintone',
                            progress: 25
                        ),
                        CardSubInfo(
                            info: 'Shine Skin',
                            progress: 100
                        ),
                      ],
                    ),
                  )
                ],
              ),
              borderRadius: const BorderRadius.vertical(top: Radius.circular(30)),
            )
          ],
        ),
      ),
    );
  }
  
}