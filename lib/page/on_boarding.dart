import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ruang_konsul_test/util/app_color.dart';
import 'package:ruang_konsul_test/router/router.gr.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({super.key});


  @override
  State<StatefulWidget> createState() => _OnBoardingScreenState();

}

class _OnBoardingScreenState extends State<OnBoardingScreen> {

  final controller = PageController();

  @override
  Widget build(BuildContext context) {

    final pages = List.generate(
        3,
        (index) => Padding(
            padding: const EdgeInsets.only(bottom: 75),
            child: Stack(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: const Image(
                    image: AssetImage('assets/illustration_boarding.png'),
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Spacer(),
                      Text(
                        'Analisa kondisi  kulitmu',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 18
                        ),
                      ),
                      Text(
                          'Lorem ipsum dolor sit amet, consectetur adipiscing',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12
                          ),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 101))
                    ],
                  ),
                )
              ],
            ),
        )
    );


    return Scaffold(
      body: Stack(
        children: [
          PageView.builder(
              controller: controller,
              itemCount: pages.length,
              itemBuilder: (_, index) {
                  return pages[index];
              }
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Padding(
                    padding: const EdgeInsets.only(top: 66, left: 24, right: 24),
                    child: Row(
                      children: [
                        SvgPicture.asset('assets/icon/cross.svg'),
                        const Flexible(
                            child: Center(
                              child: Text(
                                'Skin Analyzer',
                                style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white,
                                    fontSize: 18
                                ),
                              ),
                            )
                        )
                      ],
                    ),
                ),
                const Spacer(),
                Padding(
                    padding: const EdgeInsets.only(bottom: 36),
                    child: SmoothPageIndicator(
                      controller: controller,
                      count: pages.length,
                      effect: const WormEffect(
                          activeDotColor: AppColor.primary,
                          dotWidth: 8,
                          dotHeight: 8
                      ),
                    ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 24,  top: 24, right: 24, bottom: 36),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.vertical(top: Radius.circular(24))
                  ),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColor.primary,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)
                      )
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                            padding: const EdgeInsets.only(right: 12),
                            child: SvgPicture.asset('assets/icon/face_recognize.svg'),
                        ),
                        const Text(
                            'Mulai sekarang',
                            style: TextStyle(
                              fontWeight: FontWeight.w700
                            ),
                        )
                      ],
                    ),
                    onPressed: () => {
                      context.router.navigate(const QuestionPageRoute())
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

}