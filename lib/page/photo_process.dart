import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:ruang_konsul_test/router/router.gr.dart';
import 'package:ruang_konsul_test/util/app_color.dart';

class PhotoProcessPage extends StatefulWidget {

  final XFile picture;
  const PhotoProcessPage({super.key, required this.picture});

  @override
  State<StatefulWidget> createState() => _PhotoProcessPageState();
  
}

class _PhotoProcessPageState extends State<PhotoProcessPage> {

  @override
  void initState() {
    super.initState();
    _doResult();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: AppColor.primary,
          child: Column(
            children: [
              Padding(
                  padding: const EdgeInsets.only(top: 65, bottom: 35),
                  child: Lottie.asset('assets/animation/photo_process.json', width: 260, height: 230),
              ),
              const Text(
                  'Mohon tunggu beberapa saat..\nSim salabim abracadabra!',
                  style: TextStyle(
                    fontWeight: FontWeight.w900,
                    fontSize: 20,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }

  _doResult() async {
    Timer(
    const Duration(seconds: 5), () {
      context.router.navigate(ResultPageRoute(picture: widget.picture));
    });
  }
  
}