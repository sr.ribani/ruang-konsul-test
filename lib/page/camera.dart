import 'dart:math';

import 'package:auto_route/auto_route.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ruang_konsul_test/router/router.gr.dart';
import 'package:ruang_konsul_test/util/app_color.dart';

class CameraPage extends StatefulWidget {

  final List<CameraDescription>? cameras;
  const CameraPage({super.key, this.cameras});

  @override
  State<StatefulWidget> createState() => _CameraPageState();

}

class _CameraPageState extends State<CameraPage> {

  late CameraController _cameraController;

  @override
  void initState() {
    super.initState();
    // initialize the rear camera
    initCamera(widget.cameras![1]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: CameraPreview(_cameraController),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 18, left: 24),
                child: GestureDetector(
                  onTap: () => {
                    context.navigateBack()
                  },
                  child: Container(
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColor.black36
                    ),
                    width: 36,
                    height: 36,
                    child: Center(
                      child: SvgPicture.asset(
                        'assets/icon/cross.svg',
                        width: 14,
                        height: 14,
                      ),
                    ),
                  ),
                ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: [
                  const Padding(
                      padding: EdgeInsets.only(top: 107),
                      child: Image(
                      image: AssetImage('assets/picture_frame.png'),
                      width: 240,
                      height: 338,
                    ),
                  ),
                  const Spacer(),
                  Padding(
                      padding: const EdgeInsets.only(bottom: 34),
                      child: GestureDetector(
                        onTap: takePicture,
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.white)
                          ),
                          width: 72,
                          height: 72,
                          child: Center(
                            child: Container(
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white
                              ),
                              width: 56,
                              height: 56,
                            ),
                          ),
                        ),
                      ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _cameraController.dispose();
    super.dispose();
  }

  Future initCamera(CameraDescription cameraDescription) async {

    _cameraController = CameraController(cameraDescription, ResolutionPreset.high);

    try {
      await _cameraController.initialize().then((_) {
        if (!mounted) return;
        setState(() {});
      });
    } on CameraException catch (e) {
      debugPrint("camera error $e");
    }
  }

  Future takePicture() async {
    if (!_cameraController.value.isInitialized) {
      return null;
    }
    if (_cameraController.value.isTakingPicture) {
      return null;
    }
    try {
      await _cameraController.setFlashMode(FlashMode.off);
      XFile picture = await _cameraController.takePicture();
      context.router.navigate(PhotoProcessPageRoute(picture: picture));

    } on CameraException catch (e) {
      debugPrint('Error occured while taking picture: $e');
      return null;
    }
  }

}