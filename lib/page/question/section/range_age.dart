import 'package:flutter/cupertino.dart';
import 'package:ruang_konsul_test/component/chip.dart';

class RangeAgeSection extends StatelessWidget {
  const RangeAgeSection({super.key});



  @override
  Widget build(BuildContext context) {
    var list = [
      '18-24 Tahun',
      '25-34 Tahun',
      '35-44 Tahun',
      '45-54 Tahun',
      '55+ Tahun'
    ];

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(bottom: 18, top: 24),
          child: Text(
            'Berapa umur kamu?',
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18
            ),
          ),
        ),
        UiKitChip(list: list)
      ],
    );
  }

}