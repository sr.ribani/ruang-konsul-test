import 'package:flutter/cupertino.dart';

class TakePhotoSection extends StatelessWidget {
  const TakePhotoSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        Padding(
            padding: EdgeInsets.only(top: 74),
            child: Text(
              'Ambil foto selfie anda',
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 24
              ),
            ),
        ),
        Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 12
            ),
        ),
        Padding(
            padding: EdgeInsets.only(top: 120, bottom: 40),
            child: Image(
              image: AssetImage('assets/illustration_take_photo.png'),
              width: 224,
              height: 166,
            ),
        ),
        Text(
          'Untuk hasil yang maksimal pastikan anda berada di tempat dengan pencahayaan yang cukup',
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 12,
          ),
          textAlign: TextAlign.center,
        )
      ]
    );
  }
}
    