import 'package:flutter/cupertino.dart';
import 'package:ruang_konsul_test/component/chip.dart';

class MainProblemSkinSection extends StatelessWidget {
  const MainProblemSkinSection({super.key});



  @override
  Widget build(BuildContext context) {
    var list = [
      'Garis halus & kerutan',
      'Kulit kusam',
      'Kemerahan',
      'Pori-pori besar',
      'Pigmentasi',
      'Kulit Mengendur',
      'Noda kulit',
      'Mata bengkak',
      'Dark spots',
    ];

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
            padding: EdgeInsets.only(bottom: 18, top: 24),
            child: Text(
              'Apa masalah utama kulit kamu?',
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18
              ),
            ),
        ),
        UiKitChip(list: list)
      ],
    );
  }
  
}