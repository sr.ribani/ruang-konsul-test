import 'package:flutter/cupertino.dart';
import 'package:ruang_konsul_test/component/chip.dart';

class SkinConditionSection extends StatelessWidget {
  const SkinConditionSection({super.key});



  @override
  Widget build(BuildContext context) {
    var list = [
      'Normal',
      'Kering',
      'Lembab',
      'Sensitif',
      'Kombinasi'
    ];

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
            padding: EdgeInsets.only(bottom: 18, top: 24),
            child: Text(
              'Bagaimana kondisi kulit kamu?',
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18
              ),
            ),
        ),
        UiKitChip(list: list)
      ],
    );
  }
  
}