import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../util/app_color.dart';

class LocationLivingSection  extends StatefulWidget {
  const LocationLivingSection({super.key});

  @override
  State<StatefulWidget> createState() => _LocationLivingSectionState();

}

class _LocationLivingSectionState extends State<LocationLivingSection> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(bottom: 18, top: 24),
          child: Text(
            'Dimana lokasi anda sering menghabiskan waktu?',
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18
            ),
          ),
        ),
        TextField(
          cursorColor: AppColor.primary,
          decoration: InputDecoration(
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: AppColor.primary)
            ),
            prefix: Padding(
              padding: const EdgeInsets.only(right: 17),
              child: SizedBox(
                width: 14,
                child: SvgPicture.asset(
                  'assets/icon/pin_location.svg',
                ),
              ),
            ),
            hintText: 'Bandung, Indonesia',
            hintStyle: const TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14
            )
          ),
          style: const TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14
          ),
        )
      ],
    );
  }

}