import 'package:auto_route/auto_route.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ruang_konsul_test/component/indicator.dart';
import 'package:ruang_konsul_test/page/question/section/location_living.dart';
import 'package:ruang_konsul_test/page/question/section/main_problem_skin.dart';
import 'package:ruang_konsul_test/page/question/section/range_age.dart';
import 'package:ruang_konsul_test/page/question/section/skin_condition.dart';
import 'package:ruang_konsul_test/page/question/section/take_photo.dart';
import 'package:ruang_konsul_test/router/router.gr.dart';
import 'package:ruang_konsul_test/util/app_color.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class QuestionPage extends StatefulWidget {
  const QuestionPage({super.key});

  @override
  State<StatefulWidget> createState() => _QuestionPageState();

}

class  _QuestionPageState extends State<QuestionPage> {

  final controller = PageController();
  var selectedIndex = 0;

  var pages = const[
      SkinConditionSection(),
      MainProblemSkinSection(),
      RangeAgeSection(),
      LocationLivingSection(),
      TakePhotoSection()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.only(left: 24, right: 24, top: 56, bottom: 36),
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PageIndicator(
                  count: pages.length,
                  controller: controller,
              ),
              Flexible(
                  child: PageView.builder(
                    controller: controller,
                    itemCount: pages.length,
                    itemBuilder: (_, index) => pages[index],
                    onPageChanged: (index) {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                  )
              ),
              Visibility(
                  visible: selectedIndex != pages.length - 1,
                  child: Row(
                    children: [
                      Flexible(
                          child: TextButton(
                            child: Center(
                              child: Text(
                                selectedIndex == 0 ? 'Batalkan' : 'Kembali',
                                style: const TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontSize: 16,
                                    color: AppColor.primary
                                ),
                              ),
                            ),
                            onPressed: () => {
                              if (selectedIndex == 0) {
                                context.router.navigateBack()
                              } else {
                                previousPage()
                              }
                            },
                          )
                      ),
                      Flexible(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: AppColor.primary
                            ),
                            child: const Center(
                              child: Text(
                                'Lanjutkan',
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                            onPressed: () => {
                              if (selectedIndex == pages.length - 1) {
                                // context.router.navigate(route)
                              } else {
                                nextPage()
                              }
                            },
                          )
                      ),
                    ],
                  )
              ),
              Visibility(
                  visible: selectedIndex == pages.length - 1,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: AppColor.primary,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)
                        )
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 12),
                          child: SvgPicture.asset('assets/icon/camera.svg'),
                        ),
                        const Text(
                          'Ambil foto',
                          style: TextStyle(
                              fontWeight: FontWeight.w700
                          ),
                        )
                      ],
                    ),
                    onPressed: () async {
                      await availableCameras().then((value) =>
                          context.router.navigate(CameraPageRoute(cameras: value))
                      );
                    },
                  )
              )
            ],
          ),
        ),
      ),
    );
  }

  nextPage(){
    var currPage = controller.page?.toInt();
    if (currPage != null) {
      controller.animateToPage(currPage + 1,
        duration: const Duration(milliseconds: 400),
        curve: Curves.easeIn
      );
    }
  }

  previousPage(){
    var currPage = controller.page?.toInt();
    if (currPage != null) {
      controller.animateToPage(currPage - 1,
          duration: const Duration(milliseconds: 400),
          curve: Curves.easeIn
      );
    }
  }

}