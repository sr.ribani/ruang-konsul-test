import 'package:auto_route/annotations.dart';
import 'package:ruang_konsul_test/page/camera.dart';
import 'package:ruang_konsul_test/page/on_boarding.dart';
import 'package:ruang_konsul_test/page/photo_process.dart';
import 'package:ruang_konsul_test/page/question/question.dart';
import 'package:ruang_konsul_test/page/result.dart';

@MaterialAutoRouter(
    routes: [
      AutoRoute(page: OnBoardingScreen, initial: true),
      AutoRoute(page: QuestionPage),
      AutoRoute(page: CameraPage),
      AutoRoute(page: PhotoProcessPage),
      AutoRoute(page: ResultPage),
    ]
)
class $AppRouter {}