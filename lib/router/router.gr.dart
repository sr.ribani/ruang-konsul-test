// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i6;
import 'package:camera/camera.dart' as _i8;
import 'package:flutter/material.dart' as _i7;

import '../page/camera.dart' as _i3;
import '../page/on_boarding.dart' as _i1;
import '../page/photo_process.dart' as _i4;
import '../page/question/question.dart' as _i2;
import '../page/result.dart' as _i5;

class AppRouter extends _i6.RootStackRouter {
  AppRouter([_i7.GlobalKey<_i7.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i6.PageFactory> pagesMap = {
    OnBoardingScreenRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.OnBoardingScreen(),
      );
    },
    QuestionPageRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i2.QuestionPage(),
      );
    },
    CameraPageRoute.name: (routeData) {
      final args = routeData.argsAs<CameraPageRouteArgs>(
          orElse: () => const CameraPageRouteArgs());
      return _i6.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i3.CameraPage(
          key: args.key,
          cameras: args.cameras,
        ),
      );
    },
    PhotoProcessPageRoute.name: (routeData) {
      final args = routeData.argsAs<PhotoProcessPageRouteArgs>();
      return _i6.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i4.PhotoProcessPage(
          key: args.key,
          picture: args.picture,
        ),
      );
    },
    ResultPageRoute.name: (routeData) {
      final args = routeData.argsAs<ResultPageRouteArgs>();
      return _i6.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i5.ResultPage(
          key: args.key,
          picture: args.picture,
        ),
      );
    },
  };

  @override
  List<_i6.RouteConfig> get routes => [
        _i6.RouteConfig(
          OnBoardingScreenRoute.name,
          path: '/',
        ),
        _i6.RouteConfig(
          QuestionPageRoute.name,
          path: '/question-page',
        ),
        _i6.RouteConfig(
          CameraPageRoute.name,
          path: '/camera-page',
        ),
        _i6.RouteConfig(
          PhotoProcessPageRoute.name,
          path: '/photo-process-page',
        ),
        _i6.RouteConfig(
          ResultPageRoute.name,
          path: '/result-page',
        ),
      ];
}

/// generated route for
/// [_i1.OnBoardingScreen]
class OnBoardingScreenRoute extends _i6.PageRouteInfo<void> {
  const OnBoardingScreenRoute()
      : super(
          OnBoardingScreenRoute.name,
          path: '/',
        );

  static const String name = 'OnBoardingScreenRoute';
}

/// generated route for
/// [_i2.QuestionPage]
class QuestionPageRoute extends _i6.PageRouteInfo<void> {
  const QuestionPageRoute()
      : super(
          QuestionPageRoute.name,
          path: '/question-page',
        );

  static const String name = 'QuestionPageRoute';
}

/// generated route for
/// [_i3.CameraPage]
class CameraPageRoute extends _i6.PageRouteInfo<CameraPageRouteArgs> {
  CameraPageRoute({
    _i7.Key? key,
    List<_i8.CameraDescription>? cameras,
  }) : super(
          CameraPageRoute.name,
          path: '/camera-page',
          args: CameraPageRouteArgs(
            key: key,
            cameras: cameras,
          ),
        );

  static const String name = 'CameraPageRoute';
}

class CameraPageRouteArgs {
  const CameraPageRouteArgs({
    this.key,
    this.cameras,
  });

  final _i7.Key? key;

  final List<_i8.CameraDescription>? cameras;

  @override
  String toString() {
    return 'CameraPageRouteArgs{key: $key, cameras: $cameras}';
  }
}

/// generated route for
/// [_i4.PhotoProcessPage]
class PhotoProcessPageRoute
    extends _i6.PageRouteInfo<PhotoProcessPageRouteArgs> {
  PhotoProcessPageRoute({
    _i7.Key? key,
    required _i8.XFile picture,
  }) : super(
          PhotoProcessPageRoute.name,
          path: '/photo-process-page',
          args: PhotoProcessPageRouteArgs(
            key: key,
            picture: picture,
          ),
        );

  static const String name = 'PhotoProcessPageRoute';
}

class PhotoProcessPageRouteArgs {
  const PhotoProcessPageRouteArgs({
    this.key,
    required this.picture,
  });

  final _i7.Key? key;

  final _i8.XFile picture;

  @override
  String toString() {
    return 'PhotoProcessPageRouteArgs{key: $key, picture: $picture}';
  }
}

/// generated route for
/// [_i5.ResultPage]
class ResultPageRoute extends _i6.PageRouteInfo<ResultPageRouteArgs> {
  ResultPageRoute({
    _i7.Key? key,
    required _i8.XFile picture,
  }) : super(
          ResultPageRoute.name,
          path: '/result-page',
          args: ResultPageRouteArgs(
            key: key,
            picture: picture,
          ),
        );

  static const String name = 'ResultPageRoute';
}

class ResultPageRouteArgs {
  const ResultPageRouteArgs({
    this.key,
    required this.picture,
  });

  final _i7.Key? key;

  final _i8.XFile picture;

  @override
  String toString() {
    return 'ResultPageRouteArgs{key: $key, picture: $picture}';
  }
}
